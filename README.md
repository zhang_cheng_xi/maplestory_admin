# 冒险岛online后台管理系统

​	——By：XiaoYuan

## 一、项目说明

本项目是为大型多人在线网游《冒险岛online》设计的后台管理系统，可以对玩家账号、角色、游戏世界内容、世界设置、超级管理员、现金商城等进行管理，同时也具备对后台本身的用户、权限的管理。



项目在线部署地址：http://xiaoyuanblog.com:1314/

项目gitee仓库地址：https://gitee.com/zhang_cheng_xi/maplestory_admin/tree/master/



**注意：**本项目的服务端已都部署在远程服务器上并在项目中配置好了，开发及生产下都无需更改任何接口的IP地址。

如需使用本地服务器，请更改main-dev中统一API根路径设置。

首次运行项目请先运行  `npm install`命令安装所有依赖项



**项目组长：**袁子豪

**项目成员：**王斌、李泽群、黄瑶、王巧淑

### 项目整体框架

![](./doc/袁子豪组-项目框架.xmind.png)

### 任务分配

| 任务点                | 负责人 | 时间                        |
| --------------------- | ------ | --------------------------- |
| 项目整体架构          | 袁子豪 | 2021.03.13——2021.03.14，2天 |
| 后端开发              | 袁子豪 | 2021.03.15——2021.03.18，4天 |
| 数据库设计            | 袁子豪 | 2021.03.15——2021.03.18，4天 |
| 世界管理模块          | 袁子豪 | 2021.03.15——2021.03.17，3天 |
| 登录/注册模块（紧急） | 李泽群 | 2021.03.15——2021.03.16，2天 |
| 用户管理模块          | 李泽群 | 2021.03.17——2021.03.18，2天 |
| 首页模块              | 黄瑶   | 2021.03.15——2021.03.16，2天 |
| 商城管理模块          | 黄瑶   | 2021.03.17——2021.03.18，2天 |
| 职业管理模块          | 王巧淑 | 2021.03.15——2021.03.16，2天 |
| 权限管理模块（暂定）  | 王巧淑 | 2021.03.17——2021.03.18，2天 |
| 玩家管理模块          | 王斌   | 2021.03.15——2021.03.16，2天 |
| GM管理模块            | 王斌   | 2021.03.17——2021.03.18，2天 |

**重要说明：**所有的功能模块必须在2021-03-18（本周四）18：00之前完成，在2021-03-19（本周五）8：00之前完成项目的整合、优化、生产、测试、上线。



### 开发规范说明

- 变量、方法名采用小驼峰命名规范，如“userList"、"userData"
- 禁止使用任何形式的拼音，尽量使用语义化、有意义的英文单词
- 如需创建额外组件，组件名单个单词以大写开头如：'Login'，多个单词以短横线形式如：”player-list“
- **开发过程中必须要有相关的注释说明，如一个方法的功能说明、相关变量配置说明等**

- JS结尾语句统一加上分号;（非必要，根据个人习惯而定）



### 开发注意事项

- 涉及到更改全局配置（可理解为你所负责模块文件夹之外的东西），如对其不确定的话请与我商量
- 所负责的功能模块涉及到网络请求部分如果暂时还没后端api的话请先自行mock数据
- 请开发前查看当前所在git分支，务必在我所分配的分支下进行开发、提交和推送，**切勿对其它分支进行操作**！
- 遇到代码技术层面的问题请先放一边，尽量开发出能做的部分；如遇到非代码层面问题，如项目运行、环境配置等可与我商量
- **请务必在分配的时间内完成负责的模块，如果觉得自己肯定完不成了的话请一定提出来**



### 仓库分支分配

| 负责人 | 分支名      |
| ------ | ----------- |
| 王斌   | feature-wb  |
| 李泽群 | feature-lzq |
| 黄瑶   | feature-hy  |
| 王巧淑 | feature-wqs |

**注意事项：**每次开发前请先输入 **git pull origin feature** 命令拉去最新代码，然后输入 **git merge feature**把代码合并到自己的分支上，如果遇到冲突自己判断不了的请提出来。





## 二、项目展示

### 欢迎页

![](./doc/welcome.png)



### 404页面

![](./doc/404.png)



### 首页

![](./doc/dashboard-1.png)

![](./doc/dashboard-2.png)



### 世界管理

#### 地图管理

![](./doc/map.png)

#### 怪物管理

![](./doc/mob.png)

#### 装备管理

![](./doc/eqp.png)

#### 物品管理







## 三、项目服务端接口文档

在线文档：https://www.showdoc.com.cn/xiaoyuan1314?page_id=6466663019323092

### 说明

1、项目的请求根路径为：` http://127.0.0.1:1314/api/private

2、`/register`和`/login`接口不需要访问权限

3、除以上两个接口外，其它接口均需要在请求头中携带`Authorization`身份认证字段，才能正常访问成功



### 统一状态码说明

| status | msg            |
| ------ | -------------- |
| 0      | 请求成功       |
| 202    | 数据库请求失败 |
| 401    | 身份认证失败   |
| 500    | 未知的错误     |



### 登录注册

#### 用户注册

**简要描述：**

- 用户注册接口

**请求URL：**

- `/register`

**请求方式：**

- POST

**请求体：**

| 参数名   | 必选 | 类型   | 说明                                                   |
| -------- | ---- | ------ | ------------------------------------------------------ |
| username | 是   | string | 用户名，长度3-16位，可以为数字、字母、汉字和下划线组合 |
| email    | 是   | string | 邮箱                                                   |
| mobile   | 是   | string | 手机号                                                 |
| password | 是   | string | 密码，长度6-16位，至少包含数字跟字母，可以有字符       |

**返回示例：**

```json
{
    "status": 0,
    "msg": "注册成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 用户登录

**简要描述：**

- 用户登录接口

**请求URL：**

- `/login`

**请求方式：**

- POST

**请求体：**

| 参数名   | 必选 | 类型   | 说明   |
| -------- | ---- | ------ | ------ |
| username | 是   | string | 用户名 |
| password | 是   | string | 密码   |

**返回示例：**

```json
{
    "status": 0,
    "msg": "登陆成功",
    "token": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QyIiwiaWF0IjoxNjE1ODAyMjUyLCJleHAiOjE2MTU5NzUwNTJ9.khZ9G3PWYhwTIR7CFVx1GjT7EIVv6YkB55rW-g7x11o"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| token  | string | token字符串               |



### 用户管理

#### 后台用户列表

**简要描述：**

- 获取后台用户列表

**请求URL：**

- `/admin_users`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                                       |
| -------- | ---- | ------ | ---------------------------------------------------------- |
| search   | 否   | string | 搜索内容，可以通过关键词搜索符合条件的用户名、邮箱、手机号 |
| pagenum  | 是   | int    | 当前页码值                                                 |
| pagesize | 是   | int    | 分页大小                                                   |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取后台用户列表成功！",
    "data": [
        {
            "id": 1,
            "admin_username": "test1",
            "admin_mobile": "13177501978",
            "admin_email": "test1@qq.com",
            "admin_password": "$2a$10$15sGYHZliInL/4ygjW3P9uC2itm2JyNn5RjPEdH3FIU9s.ElPX5JO",
            "admin_status": 1,
            "role_id": 0,
            "last_login": "2021-03-15 17:10:58",
            "create_time": "2021-03-15 15:47:49",
            "last_ip": "10.36.135.79:1314",
            "create_ip": null
        },
        {
            "id": 10,
            "admin_username": "test2",
            "admin_mobile": "13177501978",
            "admin_email": "test2@qq.com",
            "admin_password": "$2a$10$pd0675WBuEvtARjbzhkYAOlS2cwVaZavHiaYi3DYB/zuANmzzU0WS",
            "admin_status": 1,
            "role_id": 0,
            "last_login": "2021-03-15 17:57:32",
            "create_time": "2021-03-15 17:55:50",
            "last_ip": "10.36.135.79:1314",
            "create_ip": "10.36.135.79:1314"
        }
    ],
    "total": 2,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 后台用户数据列表          |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找后台账号信息

**简要描述：**

- 根据ID查找后台账号

**请求URL：**

- `/adminUser/:uid`

**请求方式：**

- GET

**请求参数：**

后台用户ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取后台用户信息成功！",
    "data": {
        "id": 1,
        "admin_username": "test1",
        "admin_mobile": "13177501978",
        "admin_email": "test1@qq.com",
        "admin_password": "$2a$10$15sGYHZliInL/4ygjW3P9uC2itm2JyNn5RjPEdH3FIU9s.ElPX5JO",
        "admin_status": 1,
        "role_name": "0",
        "last_login": "2021-03-15 17:10:58",
        "create_time": "2021-03-15 15:47:49",
        "last_ip": "10.36.135.79:1314",
        "create_ip": null
    }
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | obj    | 后台用户账号信息          |



#### 编辑后台用户账号

**简要描述：**

- 编辑后台用户账号接口

**请求URL：**

- `/adminUser/:uid`

**请求方式：**

- PUT

**请求体：**

后台用户ID携带在url中

| 参数名 | 必选 | 类型   | 说明     |
| ------ | ---- | ------ | -------- |
| email  | 是   | string | 用户邮箱 |
| mobile | 是   | string | 手机号   |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改账号成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 删除后台用户账号

**简要描述：**

- 删除后台用户账号接口

**请求URL：**

- `/adminUser/:uid`

**请求方式：**

- DELETE

**请求参数：**

后台用户ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "删除后台用户账号成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 更改后台用户账号状态

**简要描述：**

- 更改后台用户账号状态接口

**请求URL：**

- `/adminUserStatus/:uid`

**请求方式：**

- PUT

**请求参数：**

后台用户ID携带在url中

| 参数名 | 必选 | 类型 | 说明                                   |
| ------ | ---- | ---- | -------------------------------------- |
| status | 是   | int  | 后台用户状态标志位，0：禁用    1：正常 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "该账号已禁用"
}
{
    "status": 0,
    "msg": "启用该账号成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



### 玩家账号管理

#### 添加玩家账号

**简要描述：**

- 添加玩家账号接口

**请求URL：**

- `/addPlayer`

**请求方式：**

- POST

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                            |
| -------- | ---- | ------ | ----------------------------------------------- |
| username | 是   | string | 玩家用户名，长度3-16位，包含英文和数字          |
| password | 是   | string | 玩家密码，6-16位,至少包含数字跟字母，可以有字符 |
| email    | 是   | string | 玩家邮箱                                        |
| mobile   | 是   | string | 玩家手机号                                      |
| mPoints  | 是   | int    | 账号点券数                                      |
| vPoints  | 是   | int    | 账号抵用券数                                    |
| vip      | 是   | int    | 账号VIP等级                                     |
| status   | 是   | int    | 玩家帐号状态，0：封禁   1：正常                 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "添加玩家账号成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 获取玩家账号列表

**简要描述：**

- 获取玩家列表接口

**请求URL：**

- `/players`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                                       |
| -------- | ---- | ------ | ---------------------------------------------------------- |
| search   | 否   | string | 搜索内容，可以通过关键词搜索符合条件的用户名、邮箱、手机号 |
| pagenum  | 是   | int    | 当前页码值                                                 |
| pagesize | 是   | int    | 分页大小                                                   |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取玩家列表成功！",
    "data": [
        {
            "id": 1,
            "player_username": "test2",
            "player_mobile": "13177501978",
            "player_email": "test2@qq.com",
            "player_password": "$2a$10$AgpPXrCSogdsUqZPRFs6Pu6vKIKYZIHEs6EKtmKWlMhzaK5Z4K/wy",
            "player_mPoints": 10000,
            "player_vPoints": 10000,
            "vip": 1,
            "gm": 0,
            "player_status": 1,
            "last_login": null,
            "last_ip": null,
            "create_time": "2021-03-16 00:42:08",
            "create_ip": "127.0.0.1:1314"
        },
        {
            "id": 2,
            "player_username": "test3",
            "player_mobile": "13177501978",
            "player_email": "test3@qq.com",
            "player_password": "$2a$10$/CpzJOSY3R5Rwdp2/ArBgu4qPnLeTvQ7TXFbbtBIq4mSUCjBofLMq",
            "player_mPoints": 20000,
            "player_vPoints": 20000,
            "vip": 0,
            "gm": 0,
            "player_status": 1,
            "last_login": null,
            "last_ip": null,
            "create_time": "2021-03-16 00:42:45",
            "create_ip": "127.0.0.1:1314"
        }
    ],
    "total": 7,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 玩家用户数据列表          |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找玩家账号信息

**简要描述：**

- 根据ID查找玩家账号

**请求URL：**

- `/players/:pid`

**请求方式：**

- GET

**请求参数：**

玩家ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取玩家信息成功！",
    "data": {
        "id": 1,
        "player_username": "test2",
        "player_mobile": "13177501978",
        "player_email": "test2@qq.com",
        "player_password": "$2a$10$AgpPXrCSogdsUqZPRFs6Pu6vKIKYZIHEs6EKtmKWlMhzaK5Z4K/wy",
        "player_mPoints": 10000,
        "player_vPoints": 10000,
        "vip": 1,
        "gm": 0,
        "player_status": 1,
        "last_login": null,
        "last_ip": null,
        "create_time": "2021-03-16 00:42:08",
        "create_ip": "127.0.0.1:1314"
    }
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | obj    | 玩家账号信息              |





#### 编辑玩家账号

**简要描述：**

- 编辑玩家账号信息接口

**请求URL：**

- `/players/:pid`

**请求方式：**

- PUT

**请求参数：**

玩家ID携带在url中

| 参数名  | 必选 | 类型 | 说明             |
| ------- | ---- | ---- | ---------------- |
| mPoints | 是   | int  | 点券数量         |
| vPoints | 是   | int  | 抵用券数量       |
| vip     | 是   | int  | VIP会员等级(0-4) |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改账号成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 删除玩家账号

**简要描述：**

- 删除玩家账号接口

**请求URL：**

- `/players/:pid`

**请求方式：**

- DELETE

**请求参数：**

玩家账号ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "删除玩家账号成功	"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 更改玩家账号状态

**简要描述：**

- 更改玩家账号状态接口

**请求URL：**

- `/switchPlayer/:pid`

**请求方式：**

- PUT

**请求参数：**

地图ID携带在url中

| 参数名       | 必选 | 类型 | 说明                               |
| ------------ | ---- | ---- | ---------------------------------- |
| playerStatus | 是   | int  | 玩家状态标志位，0：封禁    1：正常 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "账号已被封禁"
}
{
    "status": 0,
    "msg": "账号恢复成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



### 玩家角色管理

#### 获取玩家角色列表

**简要描述：**

- 获取玩家角色接口

**请求URL：**

- `/playerRoles`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词搜索玩家角色 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取玩家角色列表成功！",
    "data": [
        {
            "id": 30066,
            "accountid": 33,
            "gm": 5,
            "vip": 0,
            "name": "czccccc",
            "world": 0,
            "level": 100,
            "exp": 0,
            "str": 1000,
            "dex": 1000,
            "luk": 1000,
            "int": 1000,
            "hp": 30000,
            "mp": 1000,
            "maxhp": 50000,
            "maxmp": 1000,
            "meso": 0,
            "hpApUsed": 0,
            "mpApUsed": 0,
            "job": 130,
            "skincolor": 0,
            "gender": 0,
            "fame": 0,
            "hair": 30030,
            "face": 20000,
            "ap": 9,
            "sp": 0,
            "map": 1,
            "spawnpoint": 1,
            "party": -1,
            "buddyCapacity": 100,
            "createdate": "2010-08-30T08:31:47.000Z",
            "rank": 1,
            "rankMove": 0,
            "jobRank": 1,
            "jobRankMove": 0,
            "guildid": 0,
            "guildrank": 5,
            "messengerid": 0,
            "messengerposition": 4,
            "reborns": 0,
            "pvpkills": 0,
            "pvpdeaths": 0,
            "omokwins": 0,
            "omoklosses": 0,
            "omokties": 0,
            "matchcardwins": 0,
            "matchcardlosses": 0,
            "matchcardties": 0,
            "married": 0,
            "partnerid": 0,
            "cantalk": 0,
            "marriagequest": 0,
            "petid": 0,
            "mountlevel": 1,
            "mountexp": 0,
            "mounttiredness": 0,
            "karma": 0
        }
    ],
    "total": 2,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 玩家用户数据列表          |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找玩家角色信息

**简要描述：**

- 根据ID查找玩家角色信息接口

**请求URL：**

- `/playerRoles/:id`

**请求方式：**

- GET

**请求参数：**

玩家角色ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取角色信息成功！",
    "data": {
        "id": 30065,
        "name": "魂",
        "level": 100,
        "exp": 0,
        "str": 63,
        "dex": 4,
        "luk": 1000,
        "int": 4,
        "hp": 50,
        "mp": 50,
        "maxhp": 10000,
        "maxmp": 50,
        "meso": 13600305,
        "job": 130,
        "ap": 0,
        "sp": 0,
        "map": 60000,
        "player_username": "test2"
    }
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | obj    | 返回的查询数据            |



#### 编辑玩家角色

**简要描述：**

- 编辑玩家角色接口

**请求URL：**

- `/playerRoles/:id`

**请求方式：**

- PUT

**请求参数：**

玩家角色ID携带在url中

**请求体：**

| 参数名 | 必选 | 类型   | 说明       |
| ------ | ---- | ------ | ---------- |
| name   | 是   | string | 角色名称   |
| level  | 是   | int    | 等级       |
| exp    | 是   | int    | 经验值     |
| str    | 是   | int    | 力量       |
| dex    | 是   | int    | 敏捷       |
| int    | 是   | int    | 智力       |
| luk    | 是   | int    | 运气       |
| hp     | 是   | int    | 当前hp     |
| mp     | 是   | int    | 当前mp     |
| maxhp  | 是   | int    | 最大hp     |
| maxmp  | 是   | int    | 最大mp     |
| meso   | 是   | int    | 金币数     |
| sp     | 是   | int    | 技能点     |
| ap     | 是   | int    | 能力点     |
| job    | 是   | int    | 职业ID     |
| map    | 是   | int    | 所在地图ID |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改玩家角色成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 删除玩家角色

**简要描述：**

- 删除玩家角色接口

**请求URL：**

- `/playerRoles/:id`

**请求方式：**

- DELETE

**请求参数：**

玩家角色ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "删除玩家角色成功	"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



### VIP设置

#### 获取VIP列表信息

**简要描述：**

- 获取VIP列表信息接口

**请求URL：**

- `/vip/info`

**请求方式：**

- GET

**请求参数：**

- 无

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取VIP信息成功！",
    "data": [
        {
            "id": 1,
            "vip_level": 0,
            "vip_name": "普通用户",
            "vip_desc": null
        },
        {
            "id": 2,
            "vip_level": 1,
            "vip_name": "初级会员",
            "vip_desc": null
        },
        {
            "id": 3,
            "vip_level": 2,
            "vip_name": "中级会员",
            "vip_desc": null
        },
        {
            "id": 4,
            "vip_level": 3,
            "vip_name": "高级会员",
            "vip_desc": null
        },
        {
            "id": 5,
            "vip_level": 4,
            "vip_name": "至尊会员",
            "vip_desc": null
        }
    ]
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | array  | VIP数据列表               |



### 世界设置

#### 获取当前世界设置

**简要描述：**

- 获取地图列表接口

**请求URL：**

- `/world/setting`

**请求方式：**

- GET

**请求参数：**

- 无

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取世界设置成功！",
    "data": {
        "id": 1, 
        "exp_rate": 1,  //经验倍率
        "gold_rate": 1, //金币倍率
        "burst_rate": 1, //爆率
        "pet_rate": 1,  //宠物倍率
        "notice": "尊敬的冒险岛玩家： 《冒险岛》已正式启动网络游戏防沉迷系统。 根据国家新闻出版署相关要求，同时也为保护您的合法权益，请您配合进行实名注册。 针对未成年人的防沉迷措施，详情请见《国家新闻出版署关于防止未成年人沉迷网络游戏的通知》。"
    }
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | Object | 世界设置                  |



#### 设置经验倍数

**简要描述：**

- 设置服务器经验倍数接口

**请求URL：**

- `/world/expRate`

**请求方式：**

- PUT

**请求体：**

| 参数名  | 必选 | 类型  | 说明                            |
| ------- | ---- | ----- | ------------------------------- |
| expRate | 是   | float | 经验倍率，两位小数格式且不小于0 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "设置经验倍数成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 设置金币倍数

**简要描述：**

- 设置服务器金币倍数接口

**请求URL：**

- `/world/goldRate`

**请求方式：**

- PUT

**请求体：**

| 参数名   | 必选 | 类型  | 说明                            |
| -------- | ---- | ----- | ------------------------------- |
| goldRate | 是   | float | 金币倍率，两位小数格式且不小于0 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "设置金币倍数成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 设置物品爆率

**简要描述：**

- 设置物品掉落倍率接口

**请求URL：**

- `/world/burstRate`

**请求方式：**

- PUT

**请求体：**

| 参数名    | 必选 | 类型  | 说明                            |
| --------- | ---- | ----- | ------------------------------- |
| burstRate | 是   | float | 物品爆率，两位小数格式且不小于0 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "设置物品掉落倍数成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 设置宠物经验倍数

**简要描述：**

- 设置宠物经验倍数接口

**请求URL：**

- `/world/petRate`

**请求方式：**

- PUT

**请求体：**

| 参数名  | 必选 | 类型  | 说明                                |
| ------- | ---- | ----- | ----------------------------------- |
| petRate | 是   | float | 宠物经验倍数，两位小数格式且不小于0 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "设置宠物经验倍数成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



### 职业管理

#### 获取职业列表

**简要描述：**

- 获取职业列表接口

**请求URL：**

- `/jobList`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                               |
| -------- | ---- | ------ | ---------------------------------- |
| search   | 否   | string | 搜索内容，可以通过id、名称来搜职业 |
| pagenum  | 是   | int    | 当前页码值                         |
| pagesize | 是   | int    | 分页大小                           |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取职业列表成功！",
    "data": [
        {
            "id": 1,
            "job_id": 500,
            "job_name": "管理员"
        },
        {
            "id": 2,
            "job_id": 510,
            "job_name": "超级管理员"
        },
        {
            "id": 3,
            "job_id": 0,
            "job_name": "新手"
        }
    ],
    "path": "http://10.36.135.79:1314",
    "total": 37,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 职业列表                  |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |





### 地图管理

#### 获取地图列表

**简要描述：**

- 获取地图列表接口

**请求URL：**

- `/maplist`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                           |
| -------- | ---- | ------ | ------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词搜地图 |
| pagenum  | 是   | int    | 当前页码值                     |
| pagesize | 是   | int    | 分页大小                       |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取地图列表成功！",
    "data": [
        {
            "id": 9903,
            "map_id": "000000000",
            "map_street": "彩虹岛",
            "map_name": "蘑菇村西入口",
            "map_desc": "test",
            "map_status": 1,
            "map_img": "/map_img/000000000.img/000000000.img/miniMap.canvas.png",
            "map_bgm": "map_bgm/FirstStepMaster"
        },
        {
            "id": 9904,
            "map_id": "000010000",
            "map_street": "彩虹岛",
            "map_name": "蘑菇村",
            "map_desc": "新手出生的村落，通过完成简单的任务学习游戏的基本操作方法。",
            "map_status": 1,
            "map_img": "/map_img/000010000.img/000010000.img/miniMap.canvas.png",
            "map_bgm": "map_bgm/FloralLife"
        }
    ],
    "path": "http://127.0.0.1:1314",
    "total": 4950,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 地图列表                  |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |





#### 根据ID查找地图信息

**简要描述：**

- 根据ID查找地图信息

**请求URL：**

- `/mapinfo/:id`

**请求方式：**

- GET

**请求参数：**

地图ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取地图信息成功！",
    "data": {
        "mapId": "000000000",
        "mapStreet": "彩虹岛",
        "mapName": "蘑菇村西入口",
        "mapDesc": "",
        "mapBgm": "FirstStepMaster"
    }
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | obj    | 地图信息                  |



#### 编辑地图信息

**简要描述：**

- 修改地图信息接口

**请求URL：**

- `/editMap/:mapId`

**请求方式：**

- PUT

**请求参数：**

地图ID携带在url中

| 参数名  | 必选 | 类型   | 说明     |
| ------- | ---- | ------ | -------- |
| mapName | 是   | string | 地图名称 |
| mapDesc | 否   | string | 地图描述 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "更新地图成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 开关地图

**简要描述：**

- 更改地图开关状态接口

**请求URL：**

- `/switchMap/:mapId`

**请求方式：**

- PUT

**请求参数：**

地图ID携带在url中

| 参数名    | 必选 | 类型 | 说明           |
| --------- | ---- | ---- | -------------- |
| mapStatus | 是   | int  | 地图开关标志位 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "更改地图状态成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 删除地图

**简要描述：**

- 删除地图接口

**请求URL：**

- `/switchMap/:mapId`

**请求方式：**

- DELETE

**请求参数：**

地图ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "删除地图成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



### 怪物管理

#### 获取怪物列表

**简要描述：**

- 获取地图列表接口

**请求URL：**

- `/mobList`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词模糊搜索怪物 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取怪物列表成功！",
    "data": [
        {
            "id": 1,
            "mob_id": 100100,
            "mob_name": "绿蜗牛",
            "level": 1,
            "hp": 8,
            "mp": 0,
            "speed": -65,
            "PADamage": 12,
            "PDDamage": 0,
            "MADamage": 0,
            "MDDamage": 0,
            "acc": 20,
            "bodyAttack": 1,
            "exp": 3,
            "eva": 0,
            "pushed": 1,
            "undead": 0,
            "mob_img": "/mob_img/0100100.img/0100100.img/stand.0.png"
        },
        {
            "id": 2,
            "mob_id": 100120,
            "mob_name": "蒂诺",
            "level": 1,
            "hp": 9,
            "mp": 0,
            "speed": -40,
            "PADamage": 17,
            "PDDamage": 0,
            "MADamage": 0,
            "MDDamage": 0,
            "acc": 20,
            "bodyAttack": 1,
            "exp": 4,
            "eva": 0,
            "pushed": 1,
            "undead": 0,
            "mob_img": "/mob_img/0100120.img/0100120.img/stand.0.png"
        }
    ],
    "path": "http://10.36.135.79:1314",
    "total": 2076,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 怪物列表数据              |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找怪物信息

**简要描述：**

- 根据ID查找怪物信息

**请求URL：**

- `/mob/:mobId`

**请求方式：**

- GET

**请求参数：**

怪物ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取怪物信息成功！",
    "data": {
        "id": 6,
        "mob_id": 100124,
        "mob_name": "蒂古鲁 ",
        "level": 9,
        "hp": 100,
        "mp": 0,
        "speed": -30,
        "PADamage": 50,
        "PDDamage": 10,
        "MADamage": 0,
        "MDDamage": 15,
        "acc": 40,
        "bodyAttack": 1,
        "exp": 19,
        "eva": 1,
        "pushed": 1,
        "undead": 0,
        "mob_img": "/mob_img/0100124.img/0100124.img/stand.0.png"
    },
    "path": "http://10.36.135.79:1314"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | Object | 怪物信息                  |
| path   | string | 静态资源根路径            |



#### 编辑怪物信息

**简要描述：**

- 修改怪物信息接口

**请求URL：**

- `/mob/:mobId`

**请求方式：**

- PUT

**请求参数：**

怪物ID携带在url中

| 参数名      | 必选 | 类型   | 说明                   |
| ----------- | ---- | ------ | ---------------------- |
| editMobData | 是   | Object | 编辑怪物信息的表单数据 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改怪物信息成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 更改怪物不死族状态

**简要描述：**

- 修改怪物是否为不死族

**请求URL：**

- `/mob/undead/:mobId`

**请求方式：**

- PUT

**请求参数：**

怪物ID携带在url中

| 参数名    | 必选 | 类型 | 说明           |
| --------- | ---- | ---- | -------------- |
| mobUndead | 是   | int  | 怪物不死族标志 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "成功设置该怪物为不死族"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |





### 物品管理

#### 获取物品列表

**简要描述：**

- 获取地图列表接口

**请求URL：**

- `/itemList/catId`

**请求方式：**

- GET

**请求参数：**

分类ID携带在url中

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词模糊搜索怪物 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取物品列表成功！",
    "data": [
        {
            "id": 2,
            "item_id": "05010002",
            "item_name": "彩虹效果",
            "item_desc": "角色上面出现彩虹效果。在键盘设定可以设定为快捷键后会调整开闭。",
            "category": 1,
            "cash": 1,
            "item_icon": "/item_icon/0501.img/0501.img/05010002.info.icon.png",
            "price": null,
            "rate": null,
            "consumeHP": null,
            "consumeMP": null,
            "req_level": null,
            "max_days": null,
            "only": 0,
            "trade	_block": 0
        },
        {
            "id": 4,
            "item_id": "05010001",
            "item_name": "星月效果",
            "item_desc": "角色上面出现星月效果。在键盘设定可以设定为快捷键后会调整开闭。",
            "category": 1,
            "cash": 1,
            "item_icon": "/item_icon/0501.img/0501.img/05010001.info.icon.png",
            "price": null,
            "rate": null,
            "consumeHP": null,
            "consumeMP": null,
            "req_level": null,
            "max_days": null,
            "only": 0,
            "trade_block": 0
        }
    ],
    "path": "http://10.36.135.79:1314",
    "total": 461,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 物品列表数据              |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



### 爆率管理

#### 获取物品爆率列表

**简要描述：**

- 获取爆率列表接口

**请求URL：**

- `/burstList`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词模糊搜索怪物 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取爆率列表成功！",
    "data": [
        {
            "id": 1,
            "item_id": "2022251",
            "item_name": "枫叶棒棒糖",
            "item_icon": "/item_icon/0202.img/0202.img/02022251.info.iconRaw.png",
            "item_desc": "让人垂涎的美味糖果！ 1分钟内命中率+100。",
            "item_cat": 2,
            "mob_id": 9400567,
            "mob_name": ".",
            "mob_img": "/mob_img/9400567.img/9400567.img/stand.0.png",
            "ban": 0,
            "chance": 600
        },
        {
            "id": 6,
            "item_id": "2060000",
            "item_name": "弓矢",
            "item_icon": "/item_icon/0206.img/0206.img/02060000.info.iconRaw.png",
            "item_desc": "弓专用矢筒.",
            "item_cat": 2,
            "mob_id": 100100,
            "mob_name": "绿蜗牛",
            "mob_img": "/mob_img/0100100.img/0100100.img/stand.0.png",
            "ban": 0,
            "chance": 200
        }
    ],
    "path": "http://127.0.0.1:1314",
    "total": 6115,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 爆率列表数据              |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找爆率信息

**简要描述：**

- 根据ID查找爆率信息

**请求URL：**

- `/burst/:id`

**请求方式：**

- GET

**请求参数：**

物品爆率ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取爆率信息成功！",
    "data": {
        "id": 1,
        "item_id": "2022251",
        "item_name": "枫叶棒棒糖",
        "item_icon": "/item_icon/0202.img/0202.img/02022251.info.iconRaw.png",
        "item_desc": "让人垂涎的美味糖果！ 1分钟内命中率+100。",
        "item_cat": 2,
        "mob_id": 9400567,
        "mob_name": ".",
        "mob_img": "/mob_img/9400567.img/9400567.img/stand.0.png",
        "ban": 0,
        "chance": 600
    },
    "path": "http://127.0.0.1:1314"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | Object | 物品爆率信息              |
| path   | string | 静态资源根路径            |



#### 修改物品爆率

**简要描述：**

- 修改物品爆率接口

**请求URL：**

- `/burst/:id`

**请求方式：**

- PUT

**请求体：**

物品爆率ID携带在url中

| 参数名    | 必选 | 类型 | 说明                           |
| --------- | ---- | ---- | ------------------------------ |
| burst Num | 是   | int  | 物品爆率的基数，越大则掉率越低 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改物品爆率成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 物品掉落开关

**简要描述：**

- 更改物品掉落状态接口

**请求URL：**

- `/burstStatus/:id`

**请求方式：**

- PUT

**请求参数：**

爆率ID携带在url中

| 参数名 | 必选 | 类型 | 说明                                        |
| ------ | ---- | ---- | ------------------------------------------- |
| status | 是   | int  | 物品爆率开关标志，1：禁用掉落   0：开启掉落 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "禁止该掉落成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |





### NPC管理

#### 获取NPC列表

**简要描述：**

- 获取NPC列表接口

**请求URL：**

- `/npcList`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词模糊搜索怪物 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取npc列表成功！",
    "data": [
        {
            "id": 1805,
            "npc_id": "1001000",
            "npc_name": "赛尔文",
            "npc_func": "武器商人",
            "npc_img": "/Npc.wz/1001000.img/1001000.img/stand.0.png",
            "speak": "便宜了! 便宜了!||欢迎光临~! 哈哈哈~"
        },
        {
            "id": 1806,
            "npc_id": "1001001",
            "npc_name": "娜塔莎",
            "npc_func": "防御武器商人",
            "npc_img": "/Npc.wz/1001001.img/1001001.img/stand.0.png",
            "speak": "开张的准备都作好了…就看我的了…||大伙儿快来阿! 这里有很多好东西呀~!||我决不会容忍偷东西的行为!"
        }
    ],
    "path": "http://10.36.135.79:1314",
    "total": 1804,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | NPC列表数据               |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



### 商店管理

#### 获取NPC商店列表

**简要描述：**

- 获取NPC商店列表接口

**请求URL：**

- `/shopList`

**请求方式：**

- GET

**请求参数：**

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词模糊搜索怪物 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取商店列表成功！",
    "data": [
        {
            "shopId": 1001,
            "shopData": [
                {
                    "shopid": 1001,
                    "npcid": 2000,
                    "itemid": 5160000,
                    "price": 5000000,
                    "position": 8,
                    "item_name": "呕吐",
                    "item_desc": "在键盘设定为快捷键后按下该键，角色的表情变成呕吐的样子。",
                    "category": 1,
                    "item_icon": "/item_icon/0516.img/0516.img/05160000.info.icon.png",
                    "npc_name": "罗杰",
                    "npc_func": "",
                    "npc_img": "/Npc.wz/0002000.img/0002000.img/stand.0.png"
                },
                {
                    "shopid": 1001,
                    "npcid": 2000,
                    "itemid": 5160001,
                    "price": 5000000,
                    "position": 7,
                    "item_name": "汗",
                    "item_desc": "在键盘设定为快捷键后按下该键，角色的表情变成出汗的样子。",
                    "category": 1,
                    "item_icon": "/item_icon/0516.img/0516.img/05160001.info.icon.png",
                    "npc_name": "罗杰",
                    "npc_func": "",
                    "npc_img": "/Npc.wz/0002000.img/0002000.img/stand.0.png"
                },
                {
                    "shopid": 1001,
                    "npcid": 2000,
                    "itemid": 5160002,
                    "price": 5000000,
                    "position": 12,
                    "item_name": "哟！",
                    "item_desc": "在键盘设定为快捷键后按下该键，角色的表情变成很高兴的样子。",
                    "category": 1,
                    "item_icon": "/item_icon/0516.img/0516.img/05160002.info.icon.png",
                    "npc_name": "罗杰",
                    "npc_func": "",
                    "npc_img": "/Npc.wz/0002000.img/0002000.img/stand.0.png"
                },
                ...
            ]
        }
    ],
    "path": "http://10.36.135.79:1314",
    "total": 84,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | NPC商店列表数据           |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找商品信息

**简要描述：**

- 根据ID查找商品信息接口

**请求URL：**

- `/shopItem/:id`

**请求方式：**

- GET

**请求参数：**

商品ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取商品信息成功！",
    "data": {
        "shopitemid": 994409,
        "price": 35000,
        "position": 3,
        "npc_name": "图里卡斯",
        "item_name": "全身铠甲敏捷卷轴"
    }
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | obj    | 返回的查询数据            |



#### 修改商品信息

**简要描述：**

- 修改商品信息接口

**请求URL：**

- `/shopItem/:id`

**请求方式：**

- PUT

**请求参数：**

商城现金物品ID携带在url中

| 参数名   | 必选 | 类型 | 说明     |
| -------- | ---- | ---- | -------- |
| price    | 是   | int  | 商品价格 |
| position | 否   | int  | 商品权重 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改商品信息成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 删除商店商品

**简要描述：**

- 删除商店商品接口

**请求URL：**

- `/shopItem/:id`

**请求方式：**

- DELETE

**请求参数：**

商店商品ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "删除商品成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



### 商城管理

#### 获取商城现金物品列表

**简要描述：**

- 获取商城现金物品列表接口

**请求URL：**

- `/cashList`

**请求方式：**

- GET

**请求参数：**

分类ID携带在url中

| 参数名   | 必选 | 类型   | 说明                                 |
| -------- | ---- | ------ | ------------------------------------ |
| search   | 否   | string | 搜索内容，可以通过关键词模糊搜索怪物 |
| pagenum  | 是   | int    | 当前页码值                           |
| pagesize | 是   | int    | 分页大小                             |

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取现金商城列表成功！",
    "data": [
        {
            "id": 31091,
            "SN": "10000033",
            "ItemId": "5150000",
            "name": "射手村美发店普通会员卡",
            "desc": "在射手村美发店可以用一次的会员卡.发型随机.",
            "Price": 2100,
            "Gender": 2,
            "OnSale": 0,
            "Period": 90,
            "Count": 1,
            "Priority": 8,
            "PbCash": 0,
            "PbPoint": 0,
            "PbGift": null,
            "Class": null,
            "img": "/item_icon/0515.img/0515.img/05150000.info.icon.png"
        },
        {
            "id": 31092,
            "SN": "10000035",
            "ItemId": "5120008",
            "name": "飘枫叶",
            "desc": "在角色位置的地图上飘枫叶30秒钟。可以输入想说的消息。",
            "Price": 3000,
            "Gender": 2,
            "OnSale": 0,
            "Period": 90,
            "Count": 11,
            "Priority": 7,
            "PbCash": 0,
            "PbPoint": 0,
            "PbGift": null,
            "Class": null,
            "img": "/item_icon/0512.img/0512.img/05120008.info.icon.png"
        }
    ],
    "path": "http://10.36.135.79:1314",
    "total": 6218,
    "pagenum": 1
}
```

**返回参数说明：**

| 参数名  | 类型   | 说明                      |
| ------- | ------ | ------------------------- |
| status  | int    | 表示请求是否成功，0：成功 |
| msg     | string | 请求结果的描述信息        |
| data    | array  | 现金商城数据              |
| path    | string | 静态资源路径              |
| total   | int    | 数据总条数                |
| pagenum | int    | 分页器当前页码            |



#### 根据ID查找现金物品信息

**简要描述：**

- 根据ID查找现金物品信息

**请求URL：**

- `/cash/:id`

**请求方式：**

- GET

**请求参数：**

现金物品ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "获取现金商城物品信息成功！",
    "data": {
        "id": 31093,
        "SN": "10000034",
        "ItemId": "5151000",
        "name": "射手村染色普通会员卡",
        "desc": "在射手村美发店可以用一次的会员卡.颜色随机.",
        "Price": 2100,
        "Gender": 2,
        "OnSale": 0,
        "Period": 90,
        "Count": 1,
        "Priority": 8,
        "PbCash": 0,
        "PbPoint": 0,
        "PbGift": null,
        "Class": null,
        "img": "/item_icon/0515.img/0515.img/05151000.info.icon.png"
    },
    "path": "http://10.36.135.79:1314"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |
| data   | obj    | 现金物品信息              |



#### 编辑商城现金物品信息

**简要描述：**

- 修改商城现金物品信息接口

**请求URL：**

- `/cash/:id`

**请求方式：**

- PUT

**请求参数：**

商城现金物品ID携带在url中

| 参数名 | 必选 | 类型   | 说明         |
| ------ | ---- | ------ | ------------ |
| name   | 是   | string | 现金商品名称 |
| desc   | 否   | string | 现金商品描述 |
| price  | 是   | int    | 现金商品价格 |

**返回示例：**

```json
{
    "status": 0,
    "msg": "修改现金商品成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



#### 删除商城现金物品

**简要描述：**

- 删除商城现金物品接口

**请求URL：**

- `/cash/:id`

**请求方式：**

- DELETE

**请求参数：**

现金物品ID携带在url中

**返回示例：**

```json
{
    "status": 0,
    "msg": "删除商城现金物品成功"
}
```

**返回参数说明：**

| 参数名 | 类型   | 说明                      |
| ------ | ------ | ------------------------- |
| status | int    | 表示请求是否成功，0：成功 |
| msg    | string | 请求结果的描述信息        |



