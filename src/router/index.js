import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/welcome'
  },
  {
    path: '/login',
    component: () =>
      import(
        /* webpackChunkName: "login-home-welcome" */ '../components/login/Login.vue'
      )
  },
  {
    path: '/register',
    component: () =>
      import(
        /* webpackChunkName: "login-home-welcome" */ '../components/login/register.vue'
      )
  },
  {
    path: '/home',
    redirect: '/welcome',
    component: () =>
      import(
        /* webpackChunkName: "login-home-welcome" */ '../components/Home.vue'
      ),
    children: [
      {
        path: '/welcome',
        component: () =>
          import(
            /* webpackChunkName: "login-home-welcome" */ '../components/Welcome.vue'
          )
      },
      {
        path: '/dashboard',
        component: () =>
          import(
            /* webpackChunkName: "dashboard" */ '../components/dashboard/dashboard.vue'
          ),
        children: [
          {
            path: '',
            component: () =>
              import('../components/dashboard/dashboard-data.vue')
          }
        ]
      },
      {
        path: '/adminUsers',
        component: () =>
          import(
            /* webpackChunkName: "users" */ '../components/users/admin-users.vue'
          )
      },
      {
        path: '/adminRoles',
        component: () =>
          import(
            /* webpackChunkName: "rights" */ '../components/rights/admin-Roles.vue'
          )
      },
      {
        path: '/adminRights',
        component: () =>
          import(
            /* webpackChunkName: "rights" */ '../components/rights/admin-rights.vue'
          )
      },
      {
        path: '/gmAccount',
        component: () =>
          import(/* webpackChunkName: "gm" */ '../components/gm/gm-account.vue')
      },
      {
        path: '/gmRoles',
        component: () =>
          import(/* webpackChunkName: "gm" */ '../components/gm/gm-roles.vue')
      },
      {
        path: '/playerAccount',
        component: () =>
          import(
            /* webpackChunkName: "player" */ '../components/player/player-account.vue'
          )
      },
      {
        path: '/playerRoles',
        component: () =>
          import(
            /* webpackChunkName: "player" */ '../components/player/player-roles.vue'
          )
      },
      {
        path: '/jobs',
        component: () =>
          import(/* webpackChunkName: "job" */ '../components/job/job-list.vue')
      },
      {
        path: '/skills',
        component: () =>
          import(
            /* webpackChunkName: "job" */ '../components/job/skill-list.vue'
          )
      },
      {
        path: '/quest',
        component: () =>
          import(
            /* webpackChunkName: "world" */ '../components/world/quest.vue'
          )
      },
      {
        path: '/shop',
        component: () =>
          import(/* webpackChunkName: "world" */ '../components/world/shop.vue')
      },
      {
        path: '/npc',
        component: () =>
          import(/* webpackChunkName: "world" */ '../components/world/npc.vue')
      },
      {
        path: '/mob',
        component: () =>
          import(/* webpackChunkName: "world" */ '../components/world/mob.vue')
      },
      {
        path: '/map',
        component: () =>
          import(/* webpackChunkName: "world" */ '../components/world/map.vue')
      },
      {
        path: '/eqp',
        component: () =>
          import(/* webpackChunkName: "world" */ '../components/world/eqp.vue')
      },
      {
        path: '/items',
        component: () =>
          import(
            /* webpackChunkName: "world" */ '../components/world/items.vue'
          )
      },
      {
        path: '/pets',
        component: () =>
          import(/* webpackChunkName: "world" */ '../components/world/pet.vue')
      },
      {
        path: '/dungeon',
        component: () =>
          import(
            /* webpackChunkName: "world" */ '../components/world/dungeon.vue'
          )
      },
      {
        path: '/burst',
        component: () =>
          import(
            /* webpackChunkName: "world" */ '../components/world/burst.vue'
          )
      },
      {
        path: '/cash',
        component: () =>
          import(
            /* webpackChunkName: "cash" */ '../components/cash/cash-items.vue'
          )
      },
      {
        path: '*',
        component: () =>
          import(/* webpackChunkName: "404" */ '../components/404.vue')
      }
    ]
  },
  {
    path: '*',
    redirect: '/home/404',
    component: () =>
      import(/* webpackChunkName: "404" */ '../components/404.vue')
  }
];

const router = new VueRouter({
  routes
});

// 配置路由全局前置守卫
router.beforeEach(function (to, from, next) {
  if (to.path === '/' || to.path === '/home') {
    return next('/welcome');
  }

  if (to.path === '/login') {
    return next();
  }

  if (to.path === '/register') {
    return next();
  }

  if (!localStorage.getItem('token')) {
    return next('/login');
  }

  next();
});

export default router;
