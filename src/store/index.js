import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // 加载层参数配置
    loadingConfig: {
      target: 'body',
      fullscreen: true,
      spinner: 'el-icon-loading',
      background: 'rgba(0, 0, 0, 0.1)',
      text: '拼命加载中'
    },
    // 数值验证
    checkNum: (rule, value, callback) => {
      if (value < 0) {
        callback(new Error('请输入大于等于0的整数'));
      } else {
        callback();
      }
    },
    // 搜索关键词标红
    emphaKeyWord: (value, keyword) => {
      return value
        .toString()
        .replace(keyword, '<span style="color: red;">' + keyword + '</span>');
    }
  },
  mutations: {},
  actions: {},
  modules: {}
});
