import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// 导入elementui
import './plugins/element.js';
// 导入axios网络请求框架
import axios from 'axios';
// 导入进度条插件
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
// 导入elment相关
import { Loading, Message } from 'element-ui';

// 在vue全局上绑定axios实例对象
Vue.prototype.$axios = axios;
Vue.config.productionTip = false;
// 绑定element的loading实例对象
Vue.prototype.$loading = Loading;
var msg = Message;
var routers = router;

// 配置统一API根路径
axios.defaults.baseURL = 'http://127.0.0.1:1314/api/private';

// 配置axios请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 进度条开始
    NProgress.start();
    // 添加请求携带的token
    config.headers.Authorization = localStorage.getItem('token');
    return config;
  },
  function (err) {
    console.log(err);
  }
);

// 配置axios响应拦截器
axios.interceptors.response.use(res => {
  // 进度条结束
  NProgress.done();
  res = res.data;
  // 检查token
  if (res.status === 401 && res.msg === '身份认证失败') {
    localStorage.removeItem('token');
    return msg({
      message: res.msg,
      type: 'error',
      duration: 1500,
      onClose: () => {
        routers.push('/login');
      }
    });
  }
  return res;
});

// 定义全局时间过滤器
Vue.filter('formatTime', function (value, form) {
    function dateFormat (date, format) {
      if (typeof date === 'string') {
        var mts = date.match(/(\/Date\((\d+)\)\/)/);
        if (mts && mts.length >= 3) {
          date = parseInt(mts[2]);
        }
      }
      date = new Date(date);
      if (!date || date.toUTCString() === 'Invalid Date') {
        return '';
      }
      var map = {
        M: date.getMonth() + 1, // 月份
        d: date.getDate(), // 日
        h: date.getHours(), // 小时
        m: date.getMinutes(), // 分
        s: date.getSeconds(), // 秒
        q: Math.floor((date.getMonth() + 3) / 3), // 季度
        S: date.getMilliseconds() // 毫秒
      };
  
      format = format.replace(/([yMdhmsqS])+/g, function (all, t) {
        var v = map[t];
        if (v !== undefined) {
          if (all.length > 1) {
            v = '0' + v;
            v = v.substr(v.length - 2);
          }
          return v;
        } else if (t === 'y') {
          return (date.getFullYear() + '').substr(4 - all.length);
        }
        return all;
      });
      return format;
    }
    return dateFormat(value, form);
  });

// 定义搜索关键词着重强调过滤器
Vue.filter('emphaKeyWord', function (value, keyword) {
  return value.replace(keyword, `<span style="color: red;">${keyword}</span>`);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
